//
//  FoodTestTests.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

// Allow force unwrapping and force try in tests
// swiftlint:disable superfluous_disable_command
// swiftlint:disable force_unwrapping
// swiftlint:disable force_try

import Alamofire
@testable import FoodTest
import OHHTTPStubs
import RxSwift
import XCTest

class PlacesGatewayImpTests: XCTestCase {
    private var disposeBag: DisposeBag!
    private var expectation: XCTestExpectation!
    private var gateway: PlacesGatewayImp!

    override func setUp() {
        disposeBag = .init()

        expectation = XCTestExpectation(description: "Places repository request")

        gateway = PlacesGatewayImp(
            baseUrl: Constants.current.baseUrl,
            decoder: JSONDecoder(),
            sessionManager: SessionManager.default
        )
    }

    override func tearDown() {
        OHHTTPStubs.removeAllStubs()
    }

    func testGetPlacesSuccess() {
        stub(condition: isApiPath("/catalog")) { _ in
            fixture(
                filePath: OHPathForFile("data.json", type(of: self))!,
                headers: ["Content-Type": "application/json"]
            )
        }

        gateway.getPlaces(for: Coordinates(latitude: 55.762885, longitude: 37.597360))
            .subscribe(onSuccess: { [expectation] places in
                XCTAssertEqual(places.count, 2)
                expectation?.fulfill()
            }, onError: { [expectation] error in
                XCTFail(error.localizedDescription)
                expectation?.fulfill()
            })
            .disposed(by: disposeBag)

        wait(for: [expectation], timeout: 1.0)
    }

    func testGetPlacesError() {
        stub(condition: isApiPath("/catalog")) { _ in
            fixture(
                filePath: OHPathForFile("data.json", type(of: self))!,
                status: 500,
                headers: ["Content-Type": "application/json"]
            )
        }

        gateway.getPlaces(for: Coordinates(latitude: 55.762885, longitude: 37.597360))
            .subscribe(onSuccess: { [expectation] _ in
                XCTFail("Expected error")
                expectation?.fulfill()
            }, onError: { [expectation] _ in
                expectation?.fulfill()
            })
            .disposed(by: disposeBag)

        wait(for: [expectation], timeout: 1.0)
    }
}
