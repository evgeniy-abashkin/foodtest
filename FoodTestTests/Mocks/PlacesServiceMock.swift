//
//  PlacesServiceMock.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 03/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

@testable import FoodTest
import Foundation
import RxSwift

class PlacesServiceMock: PlacesService {
    var getPlacesResult: Single<[Place]>!

    func getPlaces(for _: Coordinates) -> Single<[Place]> {
        return getPlacesResult
    }
}
