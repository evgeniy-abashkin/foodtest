//
//  ImageLoaderMock.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 03/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

@testable import FoodTest
import Foundation
import UIKit

class ImageLoaderMock: ImageLoaderProtocol {
    var type: ImageLoaderType
    var loadImageInvoked: Bool = false
    var clearCacheInvoked: Bool = false

    init(type: ImageLoaderType) {
        self.type = type
    }

    func loadImage(with _: URL?, into _: UIImageView) {
        loadImageInvoked = true
    }

    func clearCache() {
        clearCacheInvoked = true
    }
}
