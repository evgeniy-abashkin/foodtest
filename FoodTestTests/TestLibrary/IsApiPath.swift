//
//  IsApiPath.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

// Allow force unwrapping and force try in tests
// swiftlint:disable superfluous_disable_command
// swiftlint:disable force_unwrapping
// swiftlint:disable force_try

@testable import FoodTest
import OHHTTPStubs

func isApiPath(_ path: String) -> OHHTTPStubsTestBlock {
    return isHost(Constants.current.baseUrl.host!) && isPath("/api/v2" + path)
}
