//
//  TestDataUtils.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

// Allow force unwrapping and force try in tests
// swiftlint:disable superfluous_disable_command
// swiftlint:disable force_unwrapping
// swiftlint:disable force_try

import Foundation

private class TestBundleClass {}

func testBundle() -> Bundle {
    return Bundle(for: TestBundleClass.self)
}

func testURL(_ fileName: String) -> URL {
    let fileUrl = URL(string: fileName)!
    return testBundle().url(
        forResource: fileUrl.deletingPathExtension().path,
        withExtension: fileUrl.pathExtension
    )!
}

func testData(_ fileName: String) -> Data {
    return try! Data(contentsOf: testURL(fileName))
}

func testJson<T: Codable>(_: T.Type, from fileName: String) -> T {
    let decoder = JSONDecoder()
    return try! decoder.decode(T.self, from: testData(fileName))
}
