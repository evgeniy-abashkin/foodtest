//
//  PlacesViewModelImpTests.swift
//  FoodTestTests
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

// Allow force unwrapping and force try in tests
// swiftlint:disable superfluous_disable_command
// swiftlint:disable force_unwrapping
// swiftlint:disable force_try

import Alamofire
@testable import FoodTest
import RxSwift
import UIKit
import XCTest

class PlacesViewModelImpTests: XCTestCase {
    private var disposeBag: DisposeBag!
    private var expectation: XCTestExpectation!
    private var placesService: PlacesServiceMock!
    private var imageLoaders: [ImageLoaderType: ImageLoaderMock]!
    private var viewModel: PlacesViewModelImp!
    private let urlStub = URL(string: "https://some.url")

    override func setUp() {
        disposeBag = .init()

        expectation = XCTestExpectation(description: "Places view model expectation")

        placesService = PlacesServiceMock()

        imageLoaders = [
            .kingfisher: ImageLoaderMock(type: .kingfisher),
            .nuke: ImageLoaderMock(type: .nuke),
        ]

        viewModel = PlacesViewModelImp(
            placesService: placesService,
            imageLoaders: imageLoaders
        )
    }

    override func tearDown() {}

    func testPlacesLoading() {
        placesService.getPlacesResult = Single
            .just(testJson(ServerResponse<PlacesPayload>.self, from: "data.json"))
            .map { $0.payload.foundPlaces.map { $0.place } }

        viewModel.viewIsReady()
        viewModel.places
            .drive(onNext: { [unowned self] placeCellModels in
                XCTAssertEqual(placeCellModels.count, 2)

                placeCellModels[0].imageLoader.loadImage(with: self.urlStub, into: UIImageView())
                XCTAssertTrue(self.imageLoaders[.kingfisher]!.loadImageInvoked)
                XCTAssertFalse(self.imageLoaders[.nuke]!.loadImageInvoked)

                self.expectation.fulfill()
            })
            .disposed(by: disposeBag)

        wait(for: [expectation], timeout: 1.0)
    }

    func testImageLoaderChange() {
        placesService.getPlacesResult = Single
            .just(testJson(ServerResponse<PlacesPayload>.self, from: "data.json"))
            .map { $0.payload.foundPlaces.map { $0.place } }

        viewModel.viewIsReady()
        viewModel.places
            .skip(1)
            .drive(onNext: { [unowned self] placeCellModels in
                XCTAssertEqual(placeCellModels.count, 2)

                placeCellModels[0].imageLoader.loadImage(with: self.urlStub, into: UIImageView())
                XCTAssertFalse(self.imageLoaders[.kingfisher]!.loadImageInvoked)
                XCTAssertTrue(self.imageLoaders[.nuke]!.loadImageInvoked)

                self.expectation.fulfill()
            })
            .disposed(by: disposeBag)

        viewModel.selectImageLoader(.nuke)

        wait(for: [expectation], timeout: 1.0)
    }
}
