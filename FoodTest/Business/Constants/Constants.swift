//
//  Constants.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Foundation

// swiftlint:disable force_unwrapping

enum Constants {
    case production
    case development
}

extension Constants {
    static var current: Constants {
        #if DEBUG
            return .development
        #else
            return .production
        #endif
    }
}

extension Constants {
    var imageBaseUrl: URL {
        switch self {
        case .development:
            return URL(string: "https://eda.yandex")!
        case .production:
            return URL(string: "https://eda.yandex")!
        }
    }

    var baseUrl: URL {
        switch self {
        case .development:
            return URL(string: "https://eda.yandex/api/v2")!
        case .production:
            return URL(string: "https://eda.yandex/api/v2")!
        }
    }
}
