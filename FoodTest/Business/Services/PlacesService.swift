//
//  PlacesService.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Foundation
import RxSwift

protocol PlacesService {
    func getPlaces(for coordinates: Coordinates) -> Single<[Place]>
}

// MARK: -

class PlacesServiceImp {
    private let placesGateway: PlacesGateway

    init(placesGateway: PlacesGateway) {
        self.placesGateway = placesGateway
    }
}

extension PlacesServiceImp: PlacesService {
    func getPlaces(for coordinates: Coordinates) -> Single<[Place]> {
        return placesGateway.getPlaces(for: coordinates)
    }
}
