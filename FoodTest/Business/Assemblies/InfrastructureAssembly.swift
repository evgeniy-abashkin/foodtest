//
//  InfrastructureAssembly.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Alamofire
import EasyDi

// swiftformat:disable redundantSelf

final class InfrastructureAssembly: Assembly {
    var decoder: JSONDecoder {
        return define(init: JSONDecoder())
    }

    var sessionManager: SessionManager {
        return define(init: SessionManager.default)
    }
}
