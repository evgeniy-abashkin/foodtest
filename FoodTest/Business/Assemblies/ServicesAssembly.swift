//
//  ServicesAssembly.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import EasyDi
import Foundation

// swiftformat:disable redundantSelf

final class ServicesAssembly: Assembly {
    private lazy var gatewaysAssembly: GatewaysAssembly = context.assembly()

    var placesService: PlacesService {
        return define(
            init: PlacesServiceImp(
                placesGateway: self.gatewaysAssembly.placesGateway
            )
        )
    }
}
