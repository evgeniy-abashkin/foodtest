//
//  GatewaysAssembly.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import EasyDi
import Foundation

// swiftformat:disable redundantSelf

final class GatewaysAssembly: Assembly {
    private lazy var infrastructureAssembly: InfrastructureAssembly = context.assembly()

    var placesGateway: PlacesGateway {
        return define(
            init: PlacesGatewayImp(
                baseUrl: Constants.current.baseUrl,
                decoder: self.infrastructureAssembly.decoder,
                sessionManager: self.infrastructureAssembly.sessionManager
            )
        )
    }
}
