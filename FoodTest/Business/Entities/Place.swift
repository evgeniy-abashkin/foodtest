//
//  Place.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

struct Place: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description = "footerDescription"
        case picture
    }

    let id: Int
    let name: String
    let description: String?
    let picture: Picture
}
