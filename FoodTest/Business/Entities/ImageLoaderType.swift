//
//  ImageLoaderType.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

enum ImageLoaderType: Int, CaseIterable {
    case kingfisher
    case sdWebImage
    case nuke

    var title: String {
        switch self {
        case .kingfisher:
            return "Kingfisher"
        case .sdWebImage:
            return "SDWebImage"
        case .nuke:
            return "Nuke"
        }
    }
}
