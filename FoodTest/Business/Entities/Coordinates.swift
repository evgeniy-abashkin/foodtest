//
//  Coords.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

struct Coordinates {
    let latitude: Double
    let longitude: Double
}
