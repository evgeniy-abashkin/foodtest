//
//  ServerResponse.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

struct ServerResponse<T: Codable>: Codable {
    let payload: T
}
