//
//  Picture.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Foundation

struct Picture: Codable {
    let uri: String
}

extension Picture {
    private func getImagePath(for size: Size) -> String? {
        let width = String(size.width)
        let height = String(size.height)

        return uri
            .replacingOccurrences(of: "{w}", with: width)
            .replacingOccurrences(of: "{h}", with: height)
    }

    func getImageUrl(for size: Size) -> URL? {
        return getImagePath(for: size)
            .map { Constants.current.imageBaseUrl.appendingPathComponent($0) }
    }
}
