//
//  PlacesGateway.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Alamofire
import Foundation
import RxAlamofire
import RxSwift

protocol PlacesGateway {
    func getPlaces(for coordinates: Coordinates) -> Single<[Place]>
}

// MARK: -

class PlacesGatewayImp {
    private let baseUrl: URL
    private let decoder: JSONDecoder
    private let sessionManager: SessionManager

    init(
        baseUrl: URL,
        decoder: JSONDecoder,
        sessionManager: SessionManager
    ) {
        self.baseUrl = baseUrl
        self.decoder = decoder
        self.sessionManager = sessionManager
    }
}

extension PlacesGatewayImp: PlacesGateway {
    func getPlaces(for coordinates: Coordinates) -> Single<[Place]> {
        return sessionManager.rx
            .data(
                .get,
                baseUrl.appendingPathComponent("catalog"),
                parameters: [
                    "latitude": coordinates.latitude,
                    "longitude": coordinates.longitude,
                ]
            )
            .decode(ServerResponse<PlacesPayload>.self, decoder: decoder)
            .map { $0.payload.foundPlaces.map { $0.place } }
            .asSingle()
    }
}
