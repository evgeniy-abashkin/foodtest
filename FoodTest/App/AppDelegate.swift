//
//  AppDelegate.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
