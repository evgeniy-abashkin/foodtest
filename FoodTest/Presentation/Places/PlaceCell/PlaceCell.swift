//
//  PlaceCell.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import UIKit

class PlaceCell: TableViewCell {
    private let pictureSize = Size(width: 100, height: 75)

    @IBOutlet private var placeImageView: UIImageView!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!

    func fill(with placeCellModel: PlaceCellModel) {
        if let imageUrl = placeCellModel.place.picture.getImageUrl(for: pictureSize) {
            placeCellModel.imageLoader.loadImage(with: imageUrl, into: placeImageView)
        }

        nameLabel.text = placeCellModel.place.name
        descriptionLabel.text = placeCellModel.place.description
    }
}
