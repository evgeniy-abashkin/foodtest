//
//  PlacesAssembly.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import EasyDi
import Foundation

// swiftformat:disable redundantSelf

final class PlacesAssembly: Assembly, ResolvingAssembly {
    private lazy var servicesAssembly: ServicesAssembly = context.assembly()

    func inject(into view: PlacesController) {
        defineInjection(key: "view", into: view) {
            $0.viewModel = self.viewModel
            return $0
        }
    }

    private var view: PlacesController {
        return definePlaceholder()
    }

    private var viewModel: PlacesViewModel {
        return define(
            init: PlacesViewModelImp(
                placesService: self.servicesAssembly.placesService,
                imageLoaders: self.imageLoaders
            ) as PlacesViewModel
        )
    }

    private var imageLoaders: [ImageLoaderType: ImageLoaderProtocol] {
        return [
            .kingfisher: ImageLoaderKingfisher(),
            .sdWebImage: ImageLoaderSDWebImage(),
            .nuke: ImageLoaderNuke(),
        ]
    }
}

extension PlacesController: InjectableByTag {
    typealias InstantiationAssembly = PlacesAssembly
}
