//
//  ViewController.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import RxCocoa
import RxOptional
import RxSwift
import UIKit

class PlacesController: UIViewController {
    var viewModel: PlacesViewModel!

    private let disposeBag: DisposeBag = .init()

    @IBOutlet private var tableView: UITableView!

    private lazy var imageLoaderSelector: UISegmentedControl = {
        let control = UISegmentedControl(frame: .zero)
        ImageLoaderType.allCases.forEach {
            control.insertSegment(withTitle: $0.title, at: $0.rawValue, animated: false)
        }
        control.selectedSegmentIndex = 0
        return control
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.titleView = imageLoaderSelector

        viewModel.viewIsReady()

        configureTable()

        configureBindings(viewModel: viewModel)
            .disposed(by: disposeBag)
    }

    private func configureBindings(viewModel: PlacesViewModel) -> [Disposable] {
        return [
            imageLoaderSelector.rx.value
                .map { ImageLoaderType(rawValue: $0) }
                .filterNil()
                .subscribe(onNext: viewModel.selectImageLoader),

            viewModel.places
                .drive(tableView.rx.items(cellType: PlaceCell.self)) { _, place, cell in
                    cell.fill(with: place)
                },

            viewModel.errorMessage
                .drive(rx.errorMessage),
        ]
    }

    private func configureTable() {
        tableView.register(cellType: PlaceCell.self)
    }
}
