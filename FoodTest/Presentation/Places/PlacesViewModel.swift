//
//  PlacesViewModel.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol PlacesViewModel {
    var places: Driver<[PlaceCellModel]> { get }
    var errorMessage: Driver<String> { get }
    func selectImageLoader(_ imageLoader: ImageLoaderType)
    func viewIsReady()
}

// MARK: -

class PlacesViewModelImp {
    private let disposeBag: DisposeBag = .init()

    private let placesService: PlacesService
    private let imageLoaders: [ImageLoaderType: ImageLoaderProtocol]

    private let placesStore: BehaviorRelay<[Place]> = .init(value: [])
    private let imageLoaderStore: BehaviorRelay<ImageLoaderType> = .init(value: .kingfisher)

    private let errorMessageRelay: PublishRelay<String> = .init()

    init(
        placesService: PlacesService,
        imageLoaders: [ImageLoaderType: ImageLoaderProtocol]
    ) {
        self.placesService = placesService
        self.imageLoaders = imageLoaders
    }
}

extension PlacesViewModelImp: PlacesViewModel {
    var places: Driver<[PlaceCellModel]> {
        return Observable
            .combineLatest(placesStore.asObservable(), imageLoader) { places, imageLoader -> [PlaceCellModel] in
                places.map { PlaceCellModel(place: $0, imageLoader: imageLoader) }
            }
            .asDriver(onErrorJustReturn: [])
    }

    var errorMessage: Driver<String> {
        return errorMessageRelay.asDriver(onErrorJustReturn: "")
    }

    func viewIsReady() {
        configureCacheClean()
        loadPlaces()
    }

    private func loadPlaces() {
        placesService.getPlaces(for: Coordinates(latitude: 55.762885, longitude: 37.597360))
            .subscribe(onSuccess: { [placesStore] places in
                placesStore.accept(places)
            }, onError: { [errorMessageRelay] error in
                errorMessageRelay.accept(error.localizedDescription)
            })
            .disposed(by: disposeBag)
    }

    func selectImageLoader(_ imageLoader: ImageLoaderType) {
        imageLoaderStore.accept(imageLoader)
    }

    private var imageLoader: Observable<ImageLoaderProtocol> {
        return imageLoaderStore.asObservable()
            .map { [imageLoaders] in imageLoaders[$0] }
            .filterNil()
    }

    /// Чистит кеш после переключения загрузчика изображений, предыдущим загрузчиком
    private func configureCacheClean() {
        Observable.zip(imageLoader, imageLoader.skip(1)) { prev, _ in prev }
            .skip(1)
            .observeOn(SerialDispatchQueueScheduler(internalSerialQueueName: "clearCacheQueue"))
            .subscribe(onNext: { previousImageLoader in
                previousImageLoader.clearCache()
            })
            .disposed(by: disposeBag)
    }
}
