//
//  ImageLoaderNuke.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Nuke
import UIKit

class ImageLoaderNuke: ImageLoaderProtocol {
    func loadImage(with url: URL?, into imageView: UIImageView) {
        guard let url = url else {
            return
        }

        Nuke.loadImage(with: url, into: imageView)
    }

    func clearCache() {
        DataLoader.sharedUrlCache.removeAllCachedResponses()
    }
}
