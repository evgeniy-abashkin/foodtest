//
//  ImageLoaderSDWebImage.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import SDWebImage
import UIKit

class ImageLoaderSDWebImage: ImageLoaderProtocol {
    func loadImage(with url: URL?, into imageView: UIImageView) {
        imageView.sd_setImage(with: url)
    }

    func clearCache() {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk {}
    }
}
