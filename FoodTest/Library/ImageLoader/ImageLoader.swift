//
//  ImageLoader.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import UIKit

protocol ImageLoaderProtocol {
    func loadImage(with url: URL?, into imageView: UIImageView)
    func clearCache()
}
