//
//  ImageLoaderKingfisher.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Kingfisher
import UIKit

class ImageLoaderKingfisher: ImageLoaderProtocol {
    func loadImage(with url: URL?, into imageView: UIImageView) {
        imageView.kf.setImage(with: url)
    }

    func clearCache() {
        ImageCache.default.clearDiskCache()
        ImageCache.default.clearMemoryCache()
    }
}
