//
//  ObservableType+Decode.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import RxSwift

extension ObservableType where E == Data {
    func decode<T: Decodable>(_: T.Type, decoder: JSONDecoder) -> Observable<T> {
        return map { data in
            try decoder.decode(T.self, from: data)
        }
    }
}
