//
//  Array+Disposable.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import RxSwift

extension Array where Element == Disposable {
    public func disposed(by bag: DisposeBag) {
        forEach { $0.disposed(by: bag) }
    }
}
