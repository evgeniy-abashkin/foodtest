//
//  TableViewCell+Rx.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 01/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Reusable
import RxSwift
import UIKit

class TableViewCell: UITableViewCell, NibReusable {
    var cellReuseDisposeBag: DisposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()

        cellReuseDisposeBag = DisposeBag()
    }
}
