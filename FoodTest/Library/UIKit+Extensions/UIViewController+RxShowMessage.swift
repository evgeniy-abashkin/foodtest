//
//  UIViewController+RxShowMessage.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import RxCocoa
import RxSwift

extension UIViewController {
    func showErrorMessage(_ message: String) {
        // FIXME: localize strings
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        present(alert, animated: true)
    }
}

extension Reactive where Base: UIViewController {
    var errorMessage: Binder<String> {
        return Binder(base) { view, message in
            view.showErrorMessage(message)
        }
    }
}
