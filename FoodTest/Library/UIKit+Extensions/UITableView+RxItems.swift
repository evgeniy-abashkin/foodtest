//
//  UITableView+RxItems.swift
//  FoodTest
//
//  Created by Evgeniy Abashkin on 02/10/2018.
//  Copyright © 2018 Evgeniy. All rights reserved.
//

import Reusable
import RxCocoa
import RxSwift

extension Reactive where Base: UITableView {
    func items<S: Sequence, Cell: UITableViewCell & Reusable, O: ObservableType>
    (cellType _: Cell.Type = Cell.self)
        -> (_ source: O)
        -> (_ configureCell: @escaping (Int, S.Iterator.Element, Cell) -> Void)
        -> Disposable
        where O.E == S {
            return items(cellIdentifier: Cell.reuseIdentifier, cellType: Cell.self)
        }
}
